output "name" {
  value = azurerm_resource_group.gotodata.name
}

output "id" {
  value = azurerm_resource_group.gotodata.id
}