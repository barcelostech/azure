# ----------------------------------------------------------------------------------------------------------------------
# REQUIRE A SPECIFIC TERRAFORM VERSION OR HIGHER
# This module has been updated with 0.12 syntax, which means it is no longer compatible with any versions below 0.12.
# ----------------------------------------------------------------------------------------------------------------------
terraform {
  required_version = ">= 0.12"
}

// Recurso de criação de resource group da Azure.
resource "azurerm_resource_group" "gotodata" {
  location = "${var.location}"
  name = "${var.name}"
  tags = var.tags
  timeouts {
    create = "60m"
    delete = "2h"
  }
}