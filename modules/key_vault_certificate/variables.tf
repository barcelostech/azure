variable "region" {}
variable "name" {}
variable "resource_group" {}
variable "key_vault_id" {}
variable "dns_names" {
  type    = list(string)
  default = []
}
variable "subject" {}
variable "tags" {
  type = "map"
  default = {}
}