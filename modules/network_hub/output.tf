output "subnet_gateway_id" {
  value = azurerm_subnet.subnet_gateway.id
}

output "subnet_gateway_name" {
  value = azurerm_subnet.subnet_gateway.name
}

output "subnet_private_id" {
  value = azurerm_subnet.subnet_private.id
}

output "subnet_private_name" {
  value = azurerm_subnet.subnet_private.name
}

output "virtual_network_name" {
  value = azurerm_virtual_network.gotodata.name
}

output "pip_id" {
  value = azurerm_public_ip.gotodata.id
}