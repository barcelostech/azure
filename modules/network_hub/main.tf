############ NetWork Configuration ############
# Create Securitys groups
resource "azurerm_network_security_group" "gotodata" {
  name = "${var.name}-security-group"
  location = var.region
  resource_group_name = var.resource_group
  tags = var.tags
}

#Create DDOS Protection Plan
resource "azurerm_network_ddos_protection_plan" "gotodata" {
  name = "${var.name}-ddos-protect-plan"
  location = var.region
  resource_group_name = var.resource_group
  tags = var.tags
}

# Create Vnets
resource "azurerm_virtual_network" "gotodata" {
  name = "${var.name}-vnet"
  location = var.region
  resource_group_name = var.resource_group
  address_space = [
    "10.0.0.0/16"]
  dns_servers = [
    "10.0.0.4",
    "10.0.0.5"]
  ddos_protection_plan {
    id = azurerm_network_ddos_protection_plan.gotodata.id
    enable = true
  }

  tags = var.tags
}

# Create Subnets
resource "azurerm_subnet" "subnet_gateway" {
  name = "${var.name}-gateway"
  resource_group_name = var.resource_group
  virtual_network_name = azurerm_virtual_network.gotodata.name
  address_prefix = "10.0.1.0/24"
}

resource "azurerm_subnet" "subnet_private" {
  name = "${var.name}-private"
  resource_group_name = var.resource_group
  virtual_network_name = azurerm_virtual_network.gotodata.name
  address_prefix = "10.0.2.0/24"
}

# Association security group
resource "azurerm_subnet_network_security_group_association" "gotodata" {
  subnet_id = azurerm_subnet.subnet_private.id
  network_security_group_id = azurerm_network_security_group.gotodata.id
}

# Create Dynamic Public IP Addresses
resource "azurerm_public_ip" "gotodata" {
  name                = "${var.name}-pip"
  location            =  var.region
  resource_group_name = var.resource_group
  allocation_method   = "Dynamic"

  sku = "Standard"
  tags = var.tags
}
