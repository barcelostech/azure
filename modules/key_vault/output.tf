output "id" {
  value = azurerm_key_vault.gotodata.id
}

output "tenant_id" {
  value = azurerm_key_vault.gotodata.tenant_id
}

output "principal_object_id" {
  value = data.azurerm_client_config.current.object_id
}