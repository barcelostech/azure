data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "gotodata" {
  name = "${var.name}-key-vault"
  location = var.region
  resource_group_name = var.resource_group
  tenant_id = data.azurerm_client_config.current.tenant_id

  sku_name = "standard"
  enabled_for_deployment = true
  enabled_for_disk_encryption = true
  enabled_for_template_deployment = true

  tags = var.tags
}

resource "azurerm_key_vault_access_policy" "gotodata" {
  key_vault_id = azurerm_key_vault.gotodata.id

  tenant_id = data.azurerm_client_config.current.tenant_id
  object_id = data.azurerm_client_config.current.object_id

  certificate_permissions = [
    "Create",
    "Delete",
    "DeleteIssuers",
    "Get",
    "GetIssuers",
    "Import",
    "List",
    "ListIssuers",
    "ManageContacts",
    "ManageIssuers",
    "SetIssuers",
    "Update",
    "Recover",
    "Restore",
    "Backup"
  ]

  key_permissions = [
    "backup",
    "create",
    "decrypt",
    "delete",
    "encrypt",
    "get",
    "import",
    "list",
    "purge",
    "recover",
    "restore",
    "sign",
    "unwrapKey",
    "update",
    "verify",
    "wrapKey",
  ]

  secret_permissions = [
    "backup",
    "delete",
    "get",
    "list",
    "purge",
    "recover",
    "restore",
    "set",
  ]
}