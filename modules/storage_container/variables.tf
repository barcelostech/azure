variable "storage_account" {}
variable "name" {}
variable "tags" {
  type = "map"
  default = {}
}