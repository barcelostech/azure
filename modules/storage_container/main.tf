resource "azurerm_storage_container" "gotodata" {
  name                  = "${replace(var.name,"-","")}"
  storage_account_name  = "${var.storage_account}"
  container_access_type = "private"
}