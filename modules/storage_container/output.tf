output "name" {
  value = azurerm_storage_container.gotodata.name
}

output "id" {
  value = azurerm_storage_container.gotodata.id
}