variable "resource_group" {}
variable "name" {}
variable "location" {}
variable "tags" {
  type = "map"
  default = {}
}