output "name" {
  value = azurerm_storage_account.gotodata.name
}

output "id" {
  value = azurerm_storage_account.gotodata.id
}