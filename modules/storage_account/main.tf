resource "azurerm_storage_account" "gotodata" {
  name                     = "${replace(var.name,"-","")}"
  resource_group_name      = "${var.resource_group}"
  location                 = "${var.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  timeouts {
    create = "60m"
    delete = "2h"
  }
}