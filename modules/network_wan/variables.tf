variable "region" {}
variable "name" {}
variable "resource_group" {}
variable "tags" {
  type = "map"
  default = {}
}