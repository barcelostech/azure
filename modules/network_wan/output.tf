output "virtual_hub_id" {
  value = azurerm_virtual_hub.gotodata.id
}

output "virtual_wan_id" {
  value = azurerm_virtual_wan.gotodata.id
}

output "virtual_network_id" {
  value = azurerm_virtual_network.gotodata.id
}