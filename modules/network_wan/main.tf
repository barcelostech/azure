############ NetWork Configuration ############
resource "azurerm_virtual_network" "gotodata" {
  name = "${var.name}-vnet"
  location = var.region
  resource_group_name = var.resource_group
  address_space = [
    "172.0.0.0/16"]
  tags = var.tags
}

resource "azurerm_virtual_wan" "gotodata" {
  name = "${var.name}-vwan"
  resource_group_name = var.resource_group
  location = var.region
}

resource "azurerm_virtual_hub" "gotodata" {
  name = "${var.name}-vhub"
  resource_group_name = var.resource_group
  location = var.region
  virtual_wan_id = azurerm_virtual_wan.gotodata.id
  address_prefix = "10.0.1.0/24"
}

resource "azurerm_virtual_hub_connection" "gotodata" {
  name = "${var.name}-vhub-connection"
  virtual_hub_id = azurerm_virtual_hub.gotodata.id
  remote_virtual_network_id = azurerm_virtual_network.gotodata.id
}

resource "azurerm_vpn_gateway" "gotodata" {
  name = "${var.name}-vpng"
  location = var.region
  resource_group_name = var.resource_group
  virtual_hub_id = azurerm_virtual_hub.gotodata.id
}